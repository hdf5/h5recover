import os
import logging
from typing import IO

import h5py
import pyfive

try:
    import hdf5plugin  # noqa F401
except ImportError:
    pass

logger = logging.getLogger(__name__)


def recover_dataset(
    dataset_in: pyfive.high_level.Dataset,
    full_name_in: str,
    h5group_out: h5py.Group,
    name_out: str,
) -> None:
    logger.debug("Recover dataset '%s'", full_name_in)
    try:
        data = dataset_in.get_data()
    except Exception as e:  # reference, vlen_strings
        try:
            dtype = dataset_in.dtype
        except Exception:
            dtype = "???"
        logger.warning("Skipped '%s' (dtype=%s): %s", full_name_in, dtype, e)
        return
    h5group_out.create_dataset(
        name_out,
        dataset_in.shape,
        dtype=dataset_in.dtype,
        compression=dataset_in.compression,
        data=data,
    )


def recover_group(
    fdesc_in: IO[bytes],
    h5group_in: pyfive.high_level.Group,
    full_name_in: str,
    h5group_out: h5py.Group,
    name_out: str,
) -> None:
    children = list(h5group_in.get_links().items())
    logger.debug(
        "Recover group '%s' (detected %d children)", full_name_in, len(children)
    )

    if name_out != "/":
        h5group_out = h5group_out.create_group(name_out)

    for name, offset in children:
        full_name_in_child = full_name_in + name + "/"
        if isinstance(offset, str):
            h5group_out[name] = h5py.SoftLink(offset)
        else:
            try:
                recover_objects(fdesc_in, offset, full_name_in_child, h5group_out, name)
            except pyfive.core.InvalidHDF5File as e:
                logger.error("Cannot read '%s': %s", full_name_in, e, exc_info=True)
                try:
                    del h5group_out[name]
                except KeyError:
                    pass


def recover_objects(
    fdesc_in: IO[bytes],
    offset: int,
    full_name_in: str,
    h5group_out: h5py.Group,
    name_out: str,
) -> None:
    try:
        pyfive_obj = pyfive.high_level.DataObjects(fdesc_in, offset)
    except Exception as e:
        logger.error("Cannot read '%s': %s", name_out, e, exc_info=True)
        return

    if pyfive_obj.is_dataset:
        recover_dataset(pyfive_obj, full_name_in, h5group_out, name_out)
    else:
        recover_group(fdesc_in, pyfive_obj, full_name_in, h5group_out, name_out)


def recover(source_name, output_mode: str = "w-", output_local: bool = False) -> None:
    if output_local:
        dirname = "."
    else:
        dirname = os.path.dirname(source_name)
    basename, ext = os.path.splitext(os.path.basename(source_name))
    dest_name = os.path.join(dirname, basename + ".recover" + ext)
    logger.info("Recover '%s'", source_name)
    with open(source_name, "rb") as fdesc_in:
        sb = pyfive.high_level.SuperBlock(fdesc_in, 0)
        logger.debug("Superblock version: %s", sb.version)
        with h5py.File(dest_name, mode=output_mode) as h5file_out:
            recover_objects(fdesc_in, sb.offset_to_dataobjects, "/", h5file_out, "/")
