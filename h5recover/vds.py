import os
import re
import shutil
import numpy
import h5py
from glob import glob


def restore_bliss_lima(dataset_file, scannr, detector, beamline, subscan=1):
    dataset_dir = os.path.dirname(dataset_file)
    lima_dir = os.path.join(dataset_dir, f"scan{scannr:04d}")
    lima_files = glob(os.path.join(lima_dir, f"{detector}_*.h5"))
    lima_nrs = [
        int(re.search(f"{detector}_([0-9]+).h5", os.path.basename(s)).group(1))
        for s in lima_files
    ]

    dataset_new_file = dataset_file + ".fix"
    if os.path.exists(dataset_new_file):
        os.unlink(dataset_new_file)
    shutil.copyfile(dataset_file, dataset_new_file)

    with h5py.File(dataset_new_file, "a") as f:
        measurement = f[f"/{scannr}.{subscan}/measurement"]
        npoints = 0
        for name in measurement.keys():
            try:
                npoints = max(measurement[name].shape[0], npoints)
            except Exception:
                pass

    layout = None
    lima_dset = f"/entry_0000/ESRF-{beamline.upper()}/{detector}/data"

    for i, (lima_nr, lima_file) in enumerate(
        sorted(zip(lima_nrs, lima_files), key=lambda tpl: tpl[0])
    ):
        first = i == 0
        if first:
            with h5py.File(lima_file, "r") as f:
                dset = f[lima_dset]
                lima_dtype = dset.dtype
                lima_block_shape = dset.shape
            npointsmax = lima_block_shape[0] * len(lima_files)
            if npoints:
                npoints = min(npoints, npointsmax)
            else:
                npoints = npointsmax
            lima_shape = list(lima_block_shape)
            lima_shape[0] = npoints
            lima_shape = tuple(lima_shape)
            print("Virtual layout", lima_shape)
            layout = h5py.VirtualLayout(lima_shape, dtype=lima_dtype)

        spath = os.path.relpath(lima_file, dataset_dir)

        i0 = lima_nr * lima_block_shape[0]
        nblock = lima_block_shape[0]
        i1 = min(i0 + nblock, npoints)

        sshape = list(lima_block_shape)
        sshape[0] = i1 - i0
        sshape = tuple(sshape)

        vsource = h5py.VirtualSource(spath, lima_dset, shape=sshape, dtype=lima_dtype)

        layout[i0:i1] = vsource
        print("Virtual source", spath, sshape)

    with h5py.File(dataset_new_file, "a") as f:
        det_path = f"/{scannr}.{subscan}/instrument/{detector}"
        det = f[det_path]
        det.create_virtual_dataset("data", layout, fillvalue=numpy.nan)

    print("Fixed file:", dataset_new_file)
