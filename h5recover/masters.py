"""Utilities to recreate the Bliss master files"""

import glob
import os
import h5py
import shutil
import datetime
from typing import List, Optional, NamedTuple
from silx.io import h5py_utils
from . import __version__


def get_default_header(filename):
    return {
        "NX_class": "NXroot",
        "file_time": datetime.datetime.now().isoformat(),
        "file_name": filename,
        "HDF5_Version": h5py.version.hdf5_version,
        "h5py_version": h5py.version.version,
        "creator": "h5recover",
        "creator_version": __version__,
    }


class LinkDestination(NamedTuple):
    filename: str
    h5item: str
    linkname: str
    collection: str

    @property
    def h5link(self):
        return h5py.ExternalLink(self.filename, self.h5item)


def find_dataset_files(proposal_root: str) -> List[str]:
    files = glob.glob(os.path.join(proposal_root, "*", "*", "*.h5"))
    files.sort(key=lambda x: os.path.getmtime(x))
    return files


def find_collection_files(proposal_root: str) -> List[str]:
    files = glob.glob(os.path.join(proposal_root, "*", "*.h5"))
    files.sort(key=lambda x: os.path.getmtime(x))
    return files


def get_entries(filename: str) -> List[str]:
    try:
        with h5py_utils.File(filename) as f:
            return list(f["/"])
    except Exception:
        return set()


def get_header(filename: str) -> dict:
    try:
        with h5py_utils.File(filename) as f:
            return dict(f.attrs)
    except Exception:
        return get_default_header(filename)


def backup_file(filename):
    backup = filename + ".bak"
    if not os.path.exists(backup):
        shutil.copy(filename, backup)


def recreate_master_file(filename, links, header):
    backup_file(filename)
    recoverfile = filename + ".recover"
    with h5py_utils.File(recoverfile, "w", track_order=True) as f:
        f.attrs.update(header)
        for link in links:
            f[link.linkname] = link.h5link
    print("mv", recoverfile, filename)


def restore_master_file(filename, links):
    link_names = set(lnk.linkname for lnk in links)
    try:
        existing = set(get_entries(filename))
    except Exception:
        existing = set()
    if link_names == existing:
        return
    header = get_header(filename)
    recreate_master_file(filename, links, header)


def restore_collection_file(collection_file, links):
    collection = os.path.basename(os.path.dirname(collection_file))
    links = [lnk for lnk in links if lnk.collection == collection]
    restore_master_file(collection_file, links)


def restore_proposal_file(proposal_file, links):
    restore_master_file(proposal_file, links)


def iter_link_destinations(dataset_files: List[str], collection: Optional[str] = None):
    for filename in dataset_files:
        collection = os.path.basename(os.path.dirname(os.path.dirname(filename)))
        entries = get_entries(filename)
        collection_dataset = os.path.basename(os.path.dirname(filename))
        for entry in entries:
            yield LinkDestination(
                filename=filename,
                h5item=entry,
                linkname=f"{collection_dataset}_{entry}",
                collection=collection,
            )
