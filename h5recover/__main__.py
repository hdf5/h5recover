import sys
import logging
from glob import glob
from .h5recover import recover


log_levels = {
    "debug": logging.DEBUG,
    "info": logging.INFO,
    "warning": logging.WARNING,
    "error": logging.ERROR,
}


def main(argv=None):
    import argparse

    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(
        description="Recover data from corrupt HDF5 files", prog="h5recover"
    )

    parser.add_argument(
        "filename",
        type=str,
        help="Full name or pattern",
    )

    parser.add_argument(
        "--overwrite",
        action="store_true",
        help="Overwrite resulting file",
    )

    parser.add_argument(
        "--output-local",
        action="store_true",
        help="Save recovered data in the current working directory",
    )

    parser.add_argument(
        "--log-level",
        type=str,
        choices=list(log_levels),
        default="info",
        help="Log level",
    )

    args, _ = parser.parse_known_args(argv[1:])

    logging.basicConfig(
        level=log_levels[args.log_level],
        format="%(asctime)s - %(levelname)s - %(message)s",
    )

    files = glob(args.filename)
    output_local = args.output_local
    if args.overwrite:
        output_mode = "w"
    else:
        output_mode = "w-"
    for filename in files:
        recover(filename, output_local=output_local, output_mode=output_mode)

    return 0


if __name__ == "__main__":
    sys.exit(main())
