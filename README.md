# h5recover

Recover data from corrupt HDF5 files

## Install

```bash
pip install git+https://gitlab.esrf.fr/denolf/h5recover.git
```

## Usage

```bash
h5recover eh1_exp_2021_02_11_0001.h5
```
