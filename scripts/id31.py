from h5recover import vds

dataset_file = "/data/visitor/ihch1641/id31/20220914/GaFe_sample6/GaFe_sample6_0001/GaFe_sample6_0001.h5"
scannr = 87
detector = "p3"
beamline = "id31"
vds.restore_bliss_lima(dataset_file, scannr, detector, beamline)
