import os
from h5recover.masters import find_collection_files
from h5recover.masters import find_dataset_files
from h5recover.masters import iter_link_destinations
from h5recover.masters import restore_proposal_file
from h5recover.masters import restore_collection_file

if __name__ == "__main__":
    proposal_root = "/data/visitor/a021907/bm02/20221109/"
    proposal = "a021907"
    beamline = "bm02"
    proposal_file = os.path.join(proposal_root, f"{proposal}_{beamline}.h5")
    collection_files = find_collection_files(proposal_root)
    dataset_files = find_dataset_files(proposal_root)
    links = list(iter_link_destinations(dataset_files))

    restore_proposal_file(proposal_file, links)
    for collection_file in collection_files:
        restore_collection_file(collection_file, links)
